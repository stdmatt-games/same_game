//----------------------------------------------------------------------------//
// Others                                                                     //
//----------------------------------------------------------------------------//
const GAME_DEBUG = true;

//----------------------------------------------------------------------------//
// Random                                                                     //
//----------------------------------------------------------------------------//
const GAME_SEED = (GAME_DEBUG) ? 4 : null;

//----------------------------------------------------------------------------//
// Animation / Timing                                                         //
//----------------------------------------------------------------------------//
const ANIMATION_SPEED_MULTIPLIER = 1;

//----------------------------------------------------------------------------//
// Game Settings                                                              //
//----------------------------------------------------------------------------//
const SETTINGS_KEY_HAS_SCORE     = "menu_scene_has_scores";
const SETTINGS_KEY_SOUND_ENABLED = "sound_enabled";
const SETTINGS_KEY_BEST_SCORE    = "best_score";
const SETTINGS_KEY_LAST_SCORE    = "last_score";
